resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name              = "/aws/lambda/${var.api_name}"
  retention_in_days = 30
}

resource "aws_iam_role_policy_attachment" "cloudwatch_policy_attachment" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.cloudwatch_policy.arn
}

resource "aws_lambda_function" "lambda" {
  filename      = "../upload/lambda-api-${var.api_version}.zip"
  function_name = var.api_name
  role          = aws_iam_role.lambda_role.arn
  handler       = "index.handler"
  timeout       = 30
  memory_size   = 1024

  source_code_hash = filebase64sha256("../upload/lambda-api-${var.api_version}.zip")

  runtime = "nodejs12.x"
  publish = true

  depends_on = [
    aws_s3_bucket_object.lambda_deployment
  ]
}

resource "aws_lambda_alias" "lambda_alias_staging" {
  name             = "staging"
  description      = "Alias for API version for staging/testing (${var.api_version})"
  function_name    = aws_lambda_function.lambda.arn
  function_version = aws_lambda_function.lambda.version
  depends_on = [
    aws_lambda_function.lambda
  ]
}

resource "aws_lambda_alias" "lambda_alias_active" {
  name             = "active"
  description      = "Alias for API version for active usage"
  function_name    = aws_lambda_function.lambda.arn
  function_version = var.active_version == "LATEST" ? aws_lambda_function.lambda.version : var.active_version
  count            = var.env != "prod" ? 1 : 0
  depends_on = [
    aws_lambda_function.lambda
  ]
}
