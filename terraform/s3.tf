resource "aws_s3_bucket" "labpda_api_deployment_bucket" {
  bucket = var.deployment_bucket
  acl    = "private"

  lifecycle_rule {
    id      = "deployment_retention"
    enabled = true

    prefix = "/"

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    expiration {
      days = 60
    }
  }
}


resource "aws_s3_bucket_object" "lambda_deployment" {
  bucket = var.deployment_bucket
  key    = "${var.api_name}/${var.api_name}-${var.api_version}.zip"
  source = "../upload/lambda-api-${var.api_version}.zip"
  etag   = filemd5("../upload/lambda-api-${var.api_version}.zip")
}
