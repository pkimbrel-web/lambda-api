provider "aws" {
  region  = "us-east-1"
  profile = "personal-dev"
  version = "~> 2.0"
}

terraform {
  backend "s3" {
    region  = "us-east-1"
    bucket  = "com.paulkimbrel.dev.terraform"
    key     = "backends/us-east-1/lambda-api/infra"
    profile = "personal-dev"
  }
}

variable "API_VERSION" {}

module "lambda-api" {
  source            = "../"
  account           = "747613746747"
  region            = "us-east-1"
  env               = "dev"
  domain            = "dev.paulkimbrel.com"
  app_subdomain     = "lambda-app"
  api_name          = "lambda-api"
  deployment_bucket = "com.paulkimbrel.dev.us-east-1.deployment"
  api_version       = var.API_VERSION
  active_version    = "LATEST"
}

