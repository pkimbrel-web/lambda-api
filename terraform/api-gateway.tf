data "aws_lambda_function" "authorizer" {
  function_name = "${var.app_subdomain}-authorizer"
}


resource "aws_api_gateway_account" "lambda_rest" {
  cloudwatch_role_arn = aws_iam_role.api_gateway_cloudwatch.arn
}

resource "aws_api_gateway_rest_api" "lambda_rest" {
  name        = "lambda-api-rest"
  description = "Lambda API Example"
}

resource "aws_api_gateway_resource" "lambda_proxy" {
  rest_api_id = aws_api_gateway_rest_api.lambda_rest.id
  parent_id   = aws_api_gateway_rest_api.lambda_rest.root_resource_id
  path_part   = var.api_name
}

resource "aws_api_gateway_authorizer" "authorizer" {
  name                   = "${var.app_subdomain}-authorizer"
  rest_api_id            = aws_api_gateway_rest_api.lambda_rest.id
  authorizer_uri         = data.aws_lambda_function.authorizer.invoke_arn
  authorizer_credentials = aws_iam_role.invocation_role.arn
}

resource "aws_api_gateway_method" "lambda_method_proxy" {
  rest_api_id   = aws_api_gateway_rest_api.lambda_rest.id
  resource_id   = aws_api_gateway_resource.lambda_proxy.id
  http_method   = "ANY"
  authorization = "CUSTOM"
  authorizer_id = aws_api_gateway_authorizer.authorizer.id
}

resource "aws_api_gateway_integration" "lambda_integration" {
  rest_api_id = aws_api_gateway_rest_api.lambda_rest.id
  resource_id = aws_api_gateway_method.lambda_method_proxy.resource_id
  http_method = aws_api_gateway_method.lambda_method_proxy.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${var.region}:${var.account}:function:${var.api_name}:$${stageVariables.alias}/invocations"
}

resource "aws_lambda_permission" "lambda_permission_staging" {
  statement_id  = "AllowAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = var.api_name
  principal     = "apigateway.amazonaws.com"
  qualifier     = "staging"

  source_arn = "${aws_api_gateway_rest_api.lambda_rest.execution_arn}/*/*/*"
}

resource "aws_lambda_permission" "lambda_permission_active" {
  statement_id  = "AllowAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = var.api_name
  principal     = "apigateway.amazonaws.com"
  qualifier     = "active"

  source_arn = "${aws_api_gateway_rest_api.lambda_rest.execution_arn}/*/*/*"
}

resource "aws_api_gateway_deployment" "staging" {
  rest_api_id = aws_api_gateway_rest_api.lambda_rest.id
  stage_name  = "staging"
  depends_on  = [aws_api_gateway_integration.lambda_integration]
  variables = {
    "alias" = "staging"
  }

  triggers = {
    redeployment = sha1(join(",", list(
      jsonencode(aws_api_gateway_integration.lambda_integration),
    )))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_base_path_mapping" "staging" {
  api_id      = aws_api_gateway_rest_api.lambda_rest.id
  stage_name  = aws_api_gateway_deployment.staging.stage_name
  domain_name = "${var.app_subdomain}.staging.${var.domain}"
}


resource "aws_api_gateway_deployment" "active" {
  rest_api_id = aws_api_gateway_rest_api.lambda_rest.id
  stage_name  = "active"
  depends_on  = [aws_api_gateway_integration.lambda_integration]
  variables = {
    "alias" = "active"
  }

  triggers = {
    redeployment = sha1(join(",", list(
      jsonencode(aws_api_gateway_integration.lambda_integration),
    )))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_base_path_mapping" "active" {
  api_id      = aws_api_gateway_rest_api.lambda_rest.id
  stage_name  = aws_api_gateway_deployment.active.stage_name
  domain_name = "${var.app_subdomain}.${var.domain}"
}

resource "aws_api_gateway_method_settings" "lambda_rest_settings" {
  rest_api_id = aws_api_gateway_rest_api.lambda_rest.id
  stage_name  = "staging"
  method_path = "*/*"

  settings {
    # Enable CloudWatch logging and metrics
    metrics_enabled    = true
    data_trace_enabled = true
    logging_level      = "INFO"

    # Limit the rate of calls to prevent abuse and unwanted charges
    throttling_rate_limit  = 100
    throttling_burst_limit = 50
  }

  depends_on = [aws_api_gateway_account.lambda_rest]
}
