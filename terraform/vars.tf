variable "account" {}
variable "region" {}
variable "env" {}
variable "domain" {}
variable "app_subdomain" {}
variable "deployment_bucket" {}
variable "active_version" {}
variable "api_version" {}
variable "api_name" {}

