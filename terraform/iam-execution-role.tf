resource "aws_iam_role" "lambda_role" {
  name = "${var.api_name}-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "cloudwatch_policy" {
  name   = "${var.api_name}-role-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [{
    "Sid": "CloudWatchCreate",
    "Action": [
      "logs:CreateLogGroup"
    ],
    "Resource": "arn:aws:logs:${var.region}:${var.account}:*",
    "Effect": "Allow"
  },
  {
    "Sid": "CloudWatchPutEvents",
    "Action": [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ],
    "Resource": "arn:aws:logs:${var.region}:${var.account}:log-group:/aws/lambda/${var.api_name}:*",
    "Effect": "Allow"
  }]
}
EOF
}
