import { APIGatewayProxyEvent, APIGatewayProxyEventHeaders, APIGatewayProxyEventMultiValueHeaders, Context } from "aws-lambda"
import express from "express"
import { handler } from "../src/index"

const bodyParser = require("body-parser")
const cors = require("cors")
const jwtdecode = require("jwt-decode")


const app = express()

app.use(bodyParser.json())

app.use(cors({
  origin: 'http://localhost:3000'
}))

/**
 * parseHeaders
 * @param {*} rawHeaders 
 * 
 * An HTTP request headers object is a flat array of K1,V1,K2,V2,etc.  Seems silly.  
 * But you can actually pass multiple values per header, so I guess that's a cheap way to handle it.
 * 
 * AWS API Gateway events reduces the header array into two fields: headers and multiValueHeaders.
 * 
 * headers: A straight map of header values.  Duplicate header keys are overwritten with the last given value.
 */
const parseHeaders = (rawHeaders:string[]) => {
  var headers:APIGatewayProxyEventHeaders = {}

  for (var i=0; i<rawHeaders.length; i+=2) {
    headers[rawHeaders[i]] = rawHeaders[i+1]
  }

  return headers
}

/**
 * parseMultiValueHeaders
 * @param {*} rawHeaders 
 * 
 * An HTTP request headers object is a flat array of K1,V1,K2,V2,etc.  Seems silly.  
 * But you can actually pass multiple values per header, so I guess that's a cheap way to handle it.
 * 
 * AWS API Gateway events reduces the header array into two fields: headers and multiValueHeaders.
 * 
 * multiValueHeaders: A multi-map where each key is given an array.  Most keys will have a single-value array, 
 *     but if you have a header with multiple values, the array will contain them all.  Example: Cookie-Set
 */
const parseMultiValueHeaders = (rawHeaders:string[]) => {
  var multiValueHeaders:APIGatewayProxyEventMultiValueHeaders = {}

  for (var i=0; i<rawHeaders.length; i+=2) {
    const k = rawHeaders[i]
    const v = rawHeaders[i+1]

    const currentValues = multiValueHeaders[k]
    if (!currentValues) {
      multiValueHeaders[k] = [v]
    } else {
      multiValueHeaders[k] = [...currentValues, v]
    }
  }

  return multiValueHeaders
}

/**
 * parseQuery
 * @param {*} rawQuery 
 * 
 * An HTTP request query object is a map of mixed values - single values and arrays.
 * 
 * AWS API Gateway events reduces the query object into two fields: queryStringParameters and multiValueQueryStringParameters.
 * 
 * queryStringParameters: A straight map of header values.  Duplicate header keys are overwritten with the last given value.
 * 
 * multiValueQueryStringParameters: A multi-map where each key is given an array.  Most keys will have a single-value array, 
 *     but if you have a query key with multiple values, the array will contain them all.
 */
const parseQuery = (rawQuery:string[]) => {
  var query = Object.fromEntries(Object.entries(rawQuery).map(([k, v]) => {
    if (Array.isArray(v)) {
      return [k, v[v.length-1]]
    }
    return [k, v]
  }))

  var multiValueQuery = Object.fromEntries(Object.entries(rawQuery).map(([k, v]) => {
    if (Array.isArray(v)) {
      return [k, v]
    }
    return [k, [v]]
  }))

  return [query, multiValueQuery]
}

/**
 * isEmpty
 * @param {*} obj 
 * 
 * All the ways one can identitfy an empty object.  Kills me this isn't part of the native language.
 */
const isEmpty = (obj:any) => {
  if (!obj) return true;
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;
  if (typeof obj !== "object") return true;
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}

app.use("/", async (req, res) => {
  try {
    // Authentication access (OIDC).  If given via environment variable, overrides the HTTP headers (for development).
    const auth = process.env.access_token?process.env.access_token:req.headers.authorization
    const claims = auth?jwtdecode(auth):[]
    const headers = parseHeaders(req.rawHeaders)
    const multiValueHeaders = parseMultiValueHeaders(req.rawHeaders)
    const [queryStringParameters, multiValueQueryStringParameters] = parseQuery(req.query as any)

    const event:APIGatewayProxyEvent = {
      path: req.path,
      httpMethod: req.method,
      headers,
      multiValueHeaders,
      queryStringParameters,
      multiValueQueryStringParameters,
      isBase64Encoded: false,
      pathParameters: {},
      stageVariables: {},
      resource: "",
      requestContext: {
        accountId: "",
        apiId: "",
        protocol: "",
        httpMethod: "",
        identity: {
          accessKey: "",
          accountId: "",
          apiKey: "",
          apiKeyId: "",
          caller: "",
          cognitoAuthenticationProvider: "",
          cognitoAuthenticationType: "",
          cognitoIdentityId: "",
          cognitoIdentityPoolId: "",
          principalOrgId: "",
          sourceIp: "",
          user: "",
          userAgent: "",
          userArn: ""
        },
        path: "",
        stage: "",
        requestId: "",
        requestTimeEpoch: 0,
        resourceId: "",
        resourcePath: "",
        authorizer: claims
      },
      body: isEmpty(req.body)?null:JSON.stringify(req.body)
    }

    const context:Context = {
        "functionVersion": "$LATEST",
        callbackWaitsForEmptyEventLoop: false,
        functionName: "",
        invokedFunctionArn: "",
        memoryLimitInMB: "",
        awsRequestId: "",
        logGroupName: "",
        logStreamName: "",
        getRemainingTimeInMillis: () => 0,
        done: () => {},
        fail: () => {},
        succeed: () => {},
    }

    const data = await handler(event, context, () => {})
    
    if (data) {
      res.statusCode = data.statusCode
      res.set(data.headers)
      res.send(data.body)
    } else {
      res.statusCode = 204
      res.send()
    }
  } catch (ex) {
    res.statusCode = 500
    res.set('Content-Type', "application/json")
    res.send({
      message: ex.message
    })
  }
})

app.use(function (_, res) {
  res.status(404).send("Endpoint not found")
})

const port = process.env.PORT?process.env.PORT:80
app.listen(port, () => console.log(`Listening on ${port}`))