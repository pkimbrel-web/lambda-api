#  Lambda API Sample

This gives an example of a lambda API endpoint with a debug wrapper.  The wrapper enables you to invoke the API locally for development and debugging.  The wrapper performs a best-effort emulation of an API Gateway event payload from a given HTTP request.  The wrapper can be agumented (and is intended to) to match your development needs.

## Authentication

The wrapper assumes a typical OIDC authentication scenerio where an `authorization` header is passed containing a valid `access_token`.  As the purpose of the wrapper is for debugging the function, the access token can be an expired token - but it must be valid.

**It is assumed that an authorization gate exists in the API Gatway layer!**  What does that mean?  It is assumed that validation of the access token happens in the API Gateway and not in the lambda.  So access at the point of the lambda execution can be assumed to be valid.  If you wish to emulation authentication, you can augment the wrapper to implement this behaviour.

If no access token is given, an empty claims set is given.

### Authentication Override

If you want to test with a JWT token without having to invoke an identity provider, first, you can set an environment variable `access_token` prior to starting the wrapper.  This will deliver a JWT token directly instead of looking at the HTTP headers.  This is good for direct API invocation from a browser or testing toolsuite.

**DO NOT COMMIT ACCESS TOKENS TO GIT**

## Starting Wrapper

A one-time installation is required (also required if new dependencies are added):

`npm install`

To start wrapper:

`npm start`

